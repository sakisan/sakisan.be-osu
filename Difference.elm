module Difference exposing (difference, thresholdValues)

import Array
import List exposing (..)


difference : List ( a, comparable, comparable1 ) -> ( List ( a, comparable ), List ( a, comparable1 ), List ( a, Int ) )
difference list =
    let
        name ( x, _, _ ) =
            x

        v1 ( _, x, _ ) =
            x

        v2 ( _, _, x ) =
            x

        namev1 ( x, y, _ ) =
            ( x, y )

        namev2 ( x, _, y ) =
            ( x, y )

        ranking1 =
            (sortBy v1 >> reverse >> map namev1) list

        ranking2 =
            (sortBy v2 >> reverse >> map namev2) list

        rankDiff ( name, _, _ ) =
            ( name, rank name ranking2 - rank name ranking1 )
    in
    ( ranking1, ranking2, map rankDiff list )


rank : a -> List ( a, b ) -> Int
rank e list =
    let
        recursive : Int -> a -> List ( a, b ) -> Int
        recursive i x list =
            case list of
                [] ->
                    0

                ( x, _ ) :: tail ->
                    if x == e then
                        i
                    else
                        recursive (i + 1) x tail
    in
    recursive 0 e list


thresholdValues : Int -> List comparable -> (comparable,comparable)
thresholdValues percentage values =
    let
        sortedArray =
            values
                |> List.sort
                |> Array.fromList

    in
    (Array.get (List.length values * percentage // 100) sortedArray
        |> Maybe.withDefault 0
    ,Array.get (List.length values * (100 - percentage) // 100) sortedArray
        |> Maybe.withDefault 0
    )

