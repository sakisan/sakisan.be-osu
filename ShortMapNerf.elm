module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as Decode


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { scores : List Score
    , name : String
    }


type alias Score =
    { pp : Float
    , objects : Int
    }


init : ( Model, Cmd Msg )
init =
    ( Model [] "", Cmd.none )



-- UPDATE


type Msg
    = NewScores (Result Http.Error (List Score))
    | SetPlayer String
    | GetPlayer


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewScores (Ok list) ->
            ( model |> withScores list, Cmd.none )

        NewScores (Err err) ->
            let
                _ =
                    Debug.log "network error" err
            in
            ( model, Cmd.none )

        SetPlayer name ->
            ( model |> withName name, Cmd.none )

        GetPlayer ->
            ( model, getScores model.name )


withScores : List Score -> Model -> Model
withScores scores model =
    { model | scores = scores }


withName : String -> Model -> Model
withName name model =
    { model | name = name }



-- VIEW


view : Model -> Html Msg
view model =
    let
        ppCurrent =
            pp model.scores

        -- |> Debug.log "current"
        ppFuture =
            newPP model.scores

        -- |> Debug.log "future"
    in
    div []
        [ input [ placeholder "player", onInput SetPlayer ] []
        , button [ onClick GetPlayer ] [ text "Get" ]
        , h2 [] [ text <| "You lose " ++ toString (ppCurrent - ppFuture) ++ "pp raw" ]
        , p [] [ text "if you only have these scores, though" ]
        , table [] (headers :: List.map viewScore model.scores)
        , weightTable model.scores
        ]


weightTable : List Score -> Html Msg
weightTable scores =
    table []
        (tr []
            [ th [] [ text "" ]
            , th [] [ text "before" ]
            , th [] [ text "weighted" ]
            , th [] [ text "after" ]
            , th [] [ text "weighted" ]
            , th [] [ text "diff" ]
            ]
            :: List.map3 weightView
                (List.range 1 100)
                scores
                (nerfed scores |> sortByPP)
        )


weightView : Int -> Score -> Score -> Html Msg
weightView i a b =
    let
        aWeighted =
            weight i a.pp

        bWeighted =
            weight i b.pp
    in
    tr []
        [ td [] [ text <| "#" ++ toString i ]
        , td [] [ text <| toString a.pp ]
        , td [] [ text <| toString <| chop <| aWeighted ]
        , td [] [ text <| toString <| chop <| b.pp ]
        , td [] [ text <| toString <| chop <| bWeighted ]
        , td [] [ text <| toString <| chop <| (aWeighted - bWeighted) ]
        ]


headers : Html Msg
headers =
    tr []
        [ th [] [ text "pp" ]
        , th [] [ text "objects" ]
        , th [] [ text "%" ]
        , th [] [ text "new pp" ]
        ]


chop : Float -> Float
chop n =
    let
        precision =
            10000
    in
    (precision * n)
        |> round
        |> toFloat
        |> (\v -> v / precision)


viewScore : Score -> Html msg
viewScore score =
    let
        viewPercent combo =
            percent combo
                |> chop
    in
    tr []
        [ td [] [ text <| toString score.pp ]
        , td [] [ text <| toString score.objects ]
        , td [] [ text <| toString (viewPercent score.objects) ]
        , td [] [ text <| toString (raw score.pp score.objects) ]
        ]


bonus2000 : Int -> Float
bonus2000 objects =
    if objects > 2000 then
        0.5 * logBase 10 (toFloat objects / 2000)
    else
        0


old : Int -> Float
old objects =
    0.95 + 0.4 * Basics.min 1 (toFloat objects / 2000) + bonus2000 objects


new : Int -> Float
new objects =
    let
        bonus250 =
            if objects > 250 then
                0.1 * Basics.min 1 (toFloat (objects - 250) / 250)
            else
                0

        bonus500 =
            if objects > 500 then
                0.3 * Basics.min 1 (toFloat (objects - 500) / 1500)
            else
                0
    in
    0.55
        + 0.4
        * Basics.min 1 (toFloat objects / 250)
        + bonus250
        + bonus500
        + bonus2000 objects


percent : Int -> Float
percent objects =
    let
        calculated =
            new objects / old objects
    in
    if calculated < 0.999 then
        calculated
    else
        1


raw : Float -> Int -> Float
raw pp objects =
    percent objects * pp


nerfed : List Score -> List Score
nerfed scores =
    scores
        |> List.map (\s -> { s | pp = s.pp * percent s.objects })


newPP : List Score -> Float
newPP scores =
    scores
        |> nerfed
        |> pp


sortByPP : List Score -> List Score
sortByPP scores =
    List.sortBy .pp scores
        |> List.reverse


weight : Int -> Float -> Float
weight rank score =
    score * (0.95 ^ toFloat (rank - 1))


pp : List Score -> Float
pp scores =
    indexedFoldl
        (\i score total ->
            total + weight i score.pp
        )
        (toFloat 0)
        (sortByPP scores)


indexedFoldl : (Int -> a -> b -> b) -> b -> List a -> b
indexedFoldl func acc list =
    let
        step x ( i, acc ) =
            ( i + 1, func i x acc )
    in
    Tuple.second (List.foldl step ( 0, acc ) list)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- HTTP


getScores : String -> Cmd Msg
getScores name =
    let
        url =
            "http://sakisan.be/rest/osu?name=" ++ name
    in
    Http.send NewScores (Http.get url decodeScores)


decodeScores : Decode.Decoder (List Score)
decodeScores =
    Decode.list decodeScore


decodeScore : Decode.Decoder Score
decodeScore =
    Decode.map2 Score
        (Decode.field "pp" decodeStringToFloat)
        objectsDecoder


decodeStringToInt : Decode.Decoder Int
decodeStringToInt =
    Decode.string
        |> Decode.map (String.toInt >> Result.toMaybe >> Maybe.withDefault 0)


decodeStringToFloat : Decode.Decoder Float
decodeStringToFloat =
    Decode.string
        |> Decode.map (String.toFloat >> Result.toMaybe >> Maybe.withDefault 0)


objectsDecoder : Decode.Decoder Int
objectsDecoder =
    Decode.map4 (\a b c d -> a + b + c + d)
        (Decode.field "count50" decodeStringToInt)
        (Decode.field "count100" decodeStringToInt)
        (Decode.field "count300" decodeStringToInt)
        (Decode.field "countmiss" decodeStringToInt)
