module TestDifference exposing (..)

import Difference
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)


suite : Test
suite =
    let
        ( testV1, testV2, testDiff ) =
            Difference.difference ranking
    in
    describe "The Difference module"
        [ test "sorts by v1" <|
            \_ ->
                Expect.equal rankingV1 testV1
        , test "sorts by v2" <|
            \_ ->
                Expect.equal rankingV2 testV2
        , test "gives the rank differences" <|
            \_ ->
                Expect.equal rankingDiff testDiff
        ]


ranking : List ( String, Int, Int )
ranking =
    [ ( "Rafis", 14219, 14064 )
    , ( "Cookiezi", 14082, 14042 )
    , ( "Mathi", 13738, 13549 )
    , ( "filsdelama", 13634, 13415 )
    , ( "Vaxei", 13611, 13409 )
    , ( "Azerite", 13456, 13224 )
    , ( "Angelsim", 13434, 13186 )
    , ( "Totoki", 13116, 12931 )
    , ( "Ryuk", 12753, 12526 )
    , ( "Emilia", 12429, 12086 )
    , ( "Hvick", 12421, 12312 )
    , ( "Dustice", 12166, 12144 )
    ]


rankingV1 : List ( String, Int )
rankingV1 =
    [ ( "Rafis", 14219 )
    , ( "Cookiezi", 14082 )
    , ( "Mathi", 13738 )
    , ( "filsdelama", 13634 )
    , ( "Vaxei", 13611 )
    , ( "Azerite", 13456 )
    , ( "Angelsim", 13434 )
    , ( "Totoki", 13116 )
    , ( "Ryuk", 12753 )
    , ( "Emilia", 12429 )
    , ( "Hvick", 12421 )
    , ( "Dustice", 12166 )
    ]


rankingV2 : List ( String, Int )
rankingV2 =
    [ ( "Rafis", 14064 )
    , ( "Cookiezi", 14042 )
    , ( "Mathi", 13549 )
    , ( "filsdelama", 13415 )
    , ( "Vaxei", 13409 )
    , ( "Azerite", 13224 )
    , ( "Angelsim", 13186 )
    , ( "Totoki", 12931 )
    , ( "Ryuk", 12526 )
    , ( "Hvick", 12312 )
    , ( "Dustice", 12144 )
    , ( "Emilia", 12086 )
    ]


rankingDiff : List ( String, Int )
rankingDiff =
    [ ( "Rafis", 0 )
    , ( "Cookiezi", 0 )
    , ( "Mathi", 0 )
    , ( "filsdelama", 0 )
    , ( "Vaxei", 0 )
    , ( "Azerite", 0 )
    , ( "Angelsim", 0 )
    , ( "Totoki", 0 )
    , ( "Ryuk", 0 )
    , ( "Emilia", 2 )
    , ( "Hvick", -1 )
    , ( "Dustice", -1 )
    ]
