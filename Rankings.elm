module Main exposing (..)

import Dict exposing (Dict)
import Difference
import Html exposing (..)
import Html.Attributes exposing (..)


main : Program Model Model Msg
main =
    Html.programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    List Player


type alias Player =
    { name : String
    , id : String
    , before : Float
    , after : Float
    }


init : Model -> ( Model, Cmd Msg )
init model =
    ( model, Cmd.none )



-- UPDATE


type Msg
    = NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )



-- VIEW


view : Model -> Html Msg
view model =
    let
        ( oldRankings, newRankings, rankChange ) =
            model
                |> List.sortBy (\p -> 0 - p.after)
                |> List.map (\player -> ( player, player.before, player.after ))
                |> Difference.difference

        diffMap =
            rankChange
                |> List.map (\( player, d ) -> ( player.name, d ))
                |> Dict.fromList

        biggestRise =
            rankChange
                |> List.map Tuple.second
                |> List.minimum
                |> Maybe.withDefault 0

        biggestRises =
            rankChange
                |> List.map Tuple.second
                |> List.sort
                |> List.take 3

        biggestDrops =
            rankChange
                |> List.map Tuple.second
                |> List.sort
                |> List.reverse
                |> List.take 3

        showList list =
            list
                |> List.map toString
                |> String.join ", "

        (low,high) =
            newRankings
                |> List.map Tuple.first
                |> List.map (\player -> (player.after - player.before ))
                |> Difference.thresholdValues 5 
    in
    div []
        [ table [ class "w3-table" ]
            ([ tr []
                [ th [] [ text "player" ]
                , th [] [ text "ranks won/lost" ]
                , th [] [ text "raw change" ]
                , th [] [ text "before" ]
                , th [] [ text "after" ]
                ]
             ]
                ++ List.map2
                    (tableRows diffMap (low,high) (List.length model + biggestRise) )
                    (List.range 1 10000) 
                    newRankings
            )
        , p [] [ text <| "green: raw change above " ++ String.left 5 (toString high)]
        , p [] [ text <| "red: raw change below " ++ String.left 5 (toString low)]
        , p [] [ text <| "biggest rises: " ++ showList biggestRises ++ " ranks"]
        , p [] [ text <| "biggest drops: " ++ showList biggestDrops ++ " ranks"]
        , p [] [ text <| "?: could be lower if lower ranked players rose above"]
        ]


tableRows : Dict String Int -> (Float,Float) -> Int -> Int -> ( Player, Float ) -> Html Msg
tableRows diffMap (low,high) highestCertainRank rank ( player, newPP ) =
    let
        rankChange =
            Dict.get player.name diffMap
                |> Maybe.withDefault 0

        sign =
            if rankChange > 0 then
                "↓"
            else if rankChange < 0 then
                "↑"
            else
                ""
        rawChange =
            player.after - player.before

        rawChangeAttributes =
            if rawChange <= low then
                [ class "w3-pale-red" ]
            else if rawChange >= high then
                [ class "w3-pale-green" ]
            else
                []

        maybeQuestionMark =
            if rank < highestCertainRank then
                ""
            else "?"
    in
    tr []
        [ td [] [ a [ href (player.id ++ ".html" )] 
            [text <| "#" ++ toString rank ++ maybeQuestionMark ++ " " ++ player.name ]
            ]
        , td [] [ text <| sign ++ toString (abs rankChange) ]
        , td rawChangeAttributes [ text <| String.left 4 <| toString rawChange ]
        , td [] [ text <| toString player.before ]
        , td [] [ text <| toString player.after ]
        ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
