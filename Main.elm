module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


main : Program String Model Msg
main =
    Html.programWithFlags
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { plays : List Play
    , range : Float
    , maxWeight : Float
    }


type alias Parameters a =
    { a
        | range : Float
        , maxWeight : Float
    }


type alias Play =
    { aspect2 : Float
    , aspect2Weight : Float
    , aspect1 : Float
    , aspect1Weight : Float
    , info : String
    , newPP : Float
    , newWeight : Float
    , normalPP : Float
    , normalWeight : Float
    , aspect3 : Float
    , aspect3Weight : Float
    , pp : Float
    }


init : String -> ( Model, Cmd Msg )
init input =
    ( { plays = playsFromInput input, range = 10, maxWeight = 3 }, Cmd.none )


test : String
test =
    """
233.5  |  11.0   25.73  |  27.6   64.56  | 237   9.7   736  |  76.0   71.4   51.7  | [DoubleTime]         Shano x 40mP - Natsukoi Hanabi (Frostmourne) [Insane].osu
191.6  |   0.0    0.01  |   0.4    0.80  | 174   9.2   341  |  26.4   29.9    5.5  | []                   Nakagawa Shouko - Sorairo Days (Rizen) [Super Galaxy GIGA DRILL BREAK!!!].osu
307.0  | 100.0  306.97  | 100.0  306.97  | 202   9.7   495  | 100.0  100.0  100.0  | [DoubleTime]         Tsunamaru - Daidai Genome (Lust) [Insane].osu
"""


playsFromInput : String -> List Play
playsFromInput string =
    string
        |> String.lines
        |> List.filterMap lineToPlay


lineToPlay : String -> Maybe Play
lineToPlay string =
    case String.split "|" string of
        [ pp, normal, new, stats, weights, info ] ->
            let
                ( normalWeight, normalPP ) =
                    parse2 normal

                ( newWeight, newPP ) =
                    parse2 new

                ( aspect1, aspect2, aspect3 ) =
                    parse3 stats

                ( aspect1Weight, aspect2Weight, aspect3Weight ) =
                    parse3 weights
            in
            Just
                { aspect2 = aspect2
                , aspect2Weight = aspect2Weight
                , aspect1 = aspect1
                , aspect1Weight = aspect1Weight
                , info = info
                , newPP = newPP
                , newWeight = newWeight
                , normalPP = normalPP
                , normalWeight = normalWeight
                , aspect3 = aspect3
                , aspect3Weight = aspect3Weight
                , pp = parseFloat (String.trim pp)
                }

        _ ->
            Nothing


parseFloat : String -> Float
parseFloat =
    String.toFloat >> Result.toMaybe >> Maybe.withDefault 0


parse2 : String -> ( Float, Float )
parse2 string =
    case String.words string |> List.map parseFloat of
        [ a, b ] ->
            ( a, b )

        _ ->
            ( -1, -1 )


parse3 : String -> ( Float, Float, Float )
parse3 string =
    case String.words string |> List.map parseFloat of
        [ a, b, c ] ->
            ( a, b, c )

        _ ->
            ( -1, -1, -1 )



-- UPDATE


type Msg
    = NoOp
    | RangeUp
    | RangeDown
    | PenaltyUp
    | PenaltyDown


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        RangeUp ->
            ( { model | range = model.range + 1 }, Cmd.none )

        RangeDown ->
            ( { model | range = model.range - 1 }, Cmd.none )

        PenaltyUp ->
            ( { model | maxWeight = model.maxWeight + 1 }, Cmd.none )

        PenaltyDown ->
            ( { model | maxWeight = model.maxWeight - 1 }, Cmd.none )


markSpecialPlays : List Play -> List ( Bool, Play )
markSpecialPlays plays =
    let
        helper special list =
            case list of
                [] ->
                    []

                [ play ] ->
                    [ ( special, play ) ]

                play :: play2 :: more ->
                    ( special, play )
                        :: helper
                            ((play.newPP + 3 < play2.newPP)
                                && (play.normalWeight < play2.newWeight)
                            )
                            (play2 :: more)
    in
    helper False plays


type alias Weights =
    { aspect1 : Float
    , aspect2 : Float
    , aspect3 : Float
    }


calculateWeights : Parameters a -> List Play -> List Play
calculateWeights parameters plays =
    List.foldl (addWeightedPlay parameters) [] plays
        |> List.reverse


addWeightedPlay : Parameters a -> Play -> List Play -> List Play
addWeightedPlay parameters play previousPlays =
    let
        weights =
            List.map (weightsForMap parameters play) previousPlays
                |> List.foldl scaleAndAdd (Weights 0 0 0)

        newWeight =
            multiplier weights
    in
    { play
        | aspect1Weight = weights.aspect1
        , aspect2Weight = weights.aspect2
        , aspect3Weight = weights.aspect3
        , newWeight = newWeight * 100
        , newPP = play.pp * newWeight
    }
        :: previousPlays


weightsForMap : Parameters a -> Play -> Play -> Weights
weightsForMap parameters play previousPlay =
    { aspect1 = weight parameters previousPlay.aspect1 play.aspect1
    , aspect2 = weight parameters previousPlay.aspect2 play.aspect2
    , aspect3 = weight parameters previousPlay.aspect3 play.aspect3
    }


weight : Parameters a -> Float -> Float -> Float
weight parameters value1 value2 =
    parameters.maxWeight * (parameters.range - Basics.min parameters.range (difference value1 value2)) / parameters.range


scaleAndAdd : Weights -> Weights -> Weights
scaleAndAdd one all =
    scaleWeights (multiplier all) one
        |> addWeights all


scaleWeights : Float -> Weights -> Weights
scaleWeights multiplier weights =
    { weights
        | aspect1 = multiplier * weights.aspect1
        , aspect2 = multiplier * weights.aspect2
        , aspect3 = multiplier * weights.aspect3
    }


addWeights : Weights -> Weights -> Weights
addWeights other weights =
    { weights
        | aspect1 = other.aspect1 + weights.aspect1
        , aspect2 = other.aspect2 + weights.aspect2
        , aspect3 = other.aspect3 + weights.aspect3
    }


multiplier : Weights -> Float
multiplier weights =
    (100 - weights.aspect1 - weights.aspect2 - weights.aspect3) / 100


difference : Float -> Float -> Float
difference value1 value2 =
    abs (value1 - value2)



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "w3-responsive" ]
        [ div []
            [ text <| "similarity range: " ++ round0 model.range
            , button [ class "w3-button w3-circle w3-light-gray", onClick RangeUp ] [ text "+" ]
            , button [ class "w3-button w3-circle w3-light-gray", onClick RangeDown ] [ text "-" ]
            , text <| " penalty: " ++ round0 model.maxWeight
            , button [ class "w3-button w3-circle w3-light-gray", onClick PenaltyUp ] [ text "+" ]
            , button [ class "w3-button w3-circle w3-light-gray", onClick PenaltyDown ] [ text "-" ]
            ]
        , table [ class "w3-table" ]
            [ tbody []
                ([ tr []
                    [ th [] [ text "pp" ]
                    , th [] [ text "%" ]
                    , th [] [ text "value" ]
                    , th [] [ text "new%" ]
                    , th [] [ text "newvalue" ]
                    , th [ class "w3-right-align" ] [ text "aim" ]
                    , th [] [ text "" ]
                    , th [ class "w3-right-align" ] [ text "speed" ]
                    , th [] [ text "" ]
                    , th [ class "w3-right-align" ] [ text "acc" ]
                    , th [] [ text "" ]
                    , th [] [ text "info" ]
                    ]
                 ]
                    ++ (model.plays
                            |> List.sortBy .pp
                            |> List.reverse
                            |> calculateWeights model
                            |> markSpecialPlays
                            |> List.map viewPlay
                       )
                )
            ]
        ]


viewPlay : ( Bool, Play ) -> Html Msg
viewPlay ( special, play ) =
    let
        colorClass =
            if special then
                "w3-pale-green"
            else
                ""
    in
    tr []
        [ td [] [ text <| toString play.pp ]
        , td [] [ text <| toString play.normalWeight ]
        , td [] [ text <| toString play.normalPP ]
        , td [ class "w3-light-gray" ] [ text <| round1 play.newWeight ]
        , td [ class colorClass ] [ text <| round2 play.newPP ]
        , td [ class "w3-right-align" ] [ text <| round1 play.aspect1 ]
        , td [ class "w3-light-gray" ] [ text <| " -" ++ round1 play.aspect1Weight ++ "%" ]
        , td [ class "w3-right-align" ] [ text <| round1 play.aspect2 ]
        , td [ class "w3-light-gray" ] [ text <| " -" ++ round1 play.aspect2Weight ++ "%" ]
        , td [ class "w3-right-align" ] [ text <| round1 play.aspect3 ]
        , td [ class "w3-light-gray" ] [ text <| " -" ++ round1 play.aspect3Weight ++ "%" ]
        , td [] [ text <| play.info ]
        ]


round0 : Float -> String
round0 =
    toString << toFloat << round


round1 : Float -> String
round1 float =
    toString <| (toFloat (round (10 * float)) / 10)


round2 : Float -> String
round2 float =
    toString <| (toFloat (round (100 * float)) / 100)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
